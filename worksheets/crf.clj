;; gorilla-repl.fileformat = 1

;; **
;;; Boiler-plate code --- importing necessary things.
;; **

;; @@
(use 'nstools.ns)
(ns+ crf-in-anglican
  (:like anglican-user.worksheet))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"},{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}],"value":"[nil,nil]"}
;; <=

;; **
;;; ## Sampler from Chinese Restaurant Franchise
;;; 
;;; * @@N@@ number of restaurants.
;;; * @@M@@ number of customers per restaurant.
;;; * Unconditional sampling from CRF. 
;;; * Menu is @@CRP(\alpha)@@, table CRPs are @@CRP(\beta)@@.
;; **

;; @@
(defquery crf-sampler [N M hyperparams]
  (let [alpha (:alpha hyperparams 1.0)
        beta (:beta hyperparams 1.0)]

    (loop [menu-crp (CRP alpha)
           restaurant-crps (vec (repeatedly N #(CRP beta)))
           table-dishes (vec (repeat N {}))
           ir 0              ; restaurant
           ic 0              ; customer
           dinners []]

      (if (= ir N)
        ;; predict dinners 
        (predict :dinners dinners)
        (if (= ic M)
          ;; proceed to next restaurant
          (recur menu-crp restaurant-crps table-dishes (inc ir) 0 dinners)

          ;; seat the customer
          (let [ ;; find table for the customer
                 crp-ir (get restaurant-crps ir)
                 it (sample (produce crp-ir))
                 restaurant-crps (assoc restaurant-crps ir
                                   (absorb crp-ir it))

                 ;; if the table is new, order a dish
                 [id menu-crp] (if (contains? (get table-dishes ir) it)
                                 [(get-in table-dishes [ir it]) menu-crp]
                                 (let [id (sample (produce menu-crp))]
                                   [id (absorb menu-crp id)]))

                 ;; finally serve the dish to the customer
                 table-dishes (assoc-in table-dishes [ir it] id)
                 
                 ;; remember the allocated dinner
                 dinners (conj dinners {:restaurant ir
                                        :customer ic 
                                        :table it 
                                        :dish id})]

            ;; proceed to next customer
            (recur menu-crp restaurant-crps table-dishes 
                   ir (inc ic) dinners)))))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/crf-sampler</span>","value":"#'crf-in-anglican/crf-sampler"}
;; <=

;; **
;;;  Now, we lazily perform the inference. To specify different @@\alpha@@ and @@\beta@@, provide the third parameter, like in the commented-out line.
;; **

;; @@
(def samples (doquery :importance crf-sampler [3 10]))
; (def samples (doquery :importance crf-sampler [3 10 {:alpha 3., :beta 1.5}]))

;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/samples</span>","value":"#'crf-in-anglican/samples"}
;; <=

;; **
;;; And retrieve predicts from the lazy sequence.
;; **

;; @@
(def dinners (map get-predicts (take 10 samples)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/dinners</span>","value":"#'crf-in-anglican/dinners"}
;; <=

;; **
;;; Let's output the allocated dinners now.
;; **

;; @@
(clojure.pprint/pprint dinners)
;; @@
;; ->
;;; ({:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 4, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 5, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 6, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 7, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 8, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 9, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 1, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 2, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 3, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 4, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 5, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 6, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 7, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 8, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 9, :table 0, :dish 1}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 1, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 2, :table 1, :dish 1}
;;;    {:restaurant 2, :customer 3, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 4, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 5, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 6, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 8, :table 2, :dish 1}
;;;    {:restaurant 2, :customer 9, :table 0, :dish 2}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 5, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 1, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 2, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 3, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 4, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 5, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 6, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 7, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 8, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 9, :table 0, :dish 1}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 1}
;;;    {:restaurant 2, :customer 1, :table 0, :dish 1}
;;;    {:restaurant 2, :customer 2, :table 1, :dish 1}
;;;    {:restaurant 2, :customer 3, :table 0, :dish 1}
;;;    {:restaurant 2, :customer 4, :table 2, :dish 2}
;;;    {:restaurant 2, :customer 5, :table 2, :dish 2}
;;;    {:restaurant 2, :customer 6, :table 0, :dish 1}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 1}
;;;    {:restaurant 2, :customer 8, :table 1, :dish 1}
;;;    {:restaurant 2, :customer 9, :table 0, :dish 1}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 2, :table 2, :dish 2}
;;;    {:restaurant 0, :customer 3, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 6, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 7, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 8, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 9, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 3}
;;;    {:restaurant 1, :customer 1, :table 0, :dish 3}
;;;    {:restaurant 1, :customer 2, :table 0, :dish 3}
;;;    {:restaurant 1, :customer 3, :table 1, :dish 3}
;;;    {:restaurant 1, :customer 4, :table 2, :dish 4}
;;;    {:restaurant 1, :customer 5, :table 2, :dish 4}
;;;    {:restaurant 1, :customer 6, :table 0, :dish 3}
;;;    {:restaurant 1, :customer 7, :table 1, :dish 3}
;;;    {:restaurant 1, :customer 8, :table 1, :dish 3}
;;;    {:restaurant 1, :customer 9, :table 3, :dish 0}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 3}
;;;    {:restaurant 2, :customer 1, :table 0, :dish 3}
;;;    {:restaurant 2, :customer 2, :table 1, :dish 3}
;;;    {:restaurant 2, :customer 3, :table 2, :dish 0}
;;;    {:restaurant 2, :customer 4, :table 0, :dish 3}
;;;    {:restaurant 2, :customer 5, :table 2, :dish 0}
;;;    {:restaurant 2, :customer 6, :table 2, :dish 0}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 3}
;;;    {:restaurant 2, :customer 8, :table 0, :dish 3}
;;;    {:restaurant 2, :customer 9, :table 2, :dish 0}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 3, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 4, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 6, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 7, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 2}
;;;    {:restaurant 1, :customer 1, :table 0, :dish 2}
;;;    {:restaurant 1, :customer 2, :table 1, :dish 3}
;;;    {:restaurant 1, :customer 3, :table 1, :dish 3}
;;;    {:restaurant 1, :customer 4, :table 2, :dish 1}
;;;    {:restaurant 1, :customer 5, :table 0, :dish 2}
;;;    {:restaurant 1, :customer 6, :table 3, :dish 1}
;;;    {:restaurant 1, :customer 7, :table 1, :dish 3}
;;;    {:restaurant 1, :customer 8, :table 0, :dish 2}
;;;    {:restaurant 1, :customer 9, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 1}
;;;    {:restaurant 2, :customer 1, :table 1, :dish 1}
;;;    {:restaurant 2, :customer 2, :table 2, :dish 1}
;;;    {:restaurant 2, :customer 3, :table 0, :dish 1}
;;;    {:restaurant 2, :customer 4, :table 3, :dish 0}
;;;    {:restaurant 2, :customer 5, :table 3, :dish 0}
;;;    {:restaurant 2, :customer 6, :table 1, :dish 1}
;;;    {:restaurant 2, :customer 7, :table 3, :dish 0}
;;;    {:restaurant 2, :customer 8, :table 2, :dish 1}
;;;    {:restaurant 2, :customer 9, :table 4, :dish 1}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 1, :table 1, :dish 2}
;;;    {:restaurant 1, :customer 2, :table 2, :dish 2}
;;;    {:restaurant 1, :customer 3, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 4, :table 2, :dish 2}
;;;    {:restaurant 1, :customer 5, :table 2, :dish 2}
;;;    {:restaurant 1, :customer 6, :table 2, :dish 2}
;;;    {:restaurant 1, :customer 7, :table 2, :dish 2}
;;;    {:restaurant 1, :customer 8, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 9, :table 2, :dish 2}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 1, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 2, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 3, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 4, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 5, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 6, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 8, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 9, :table 0, :dish 2}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 1, :table 1, :dish 2}
;;;    {:restaurant 1, :customer 2, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 3, :table 2, :dish 0}
;;;    {:restaurant 1, :customer 4, :table 2, :dish 0}
;;;    {:restaurant 1, :customer 5, :table 1, :dish 2}
;;;    {:restaurant 1, :customer 6, :table 2, :dish 0}
;;;    {:restaurant 1, :customer 7, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 8, :table 2, :dish 0}
;;;    {:restaurant 1, :customer 9, :table 2, :dish 0}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 1, :table 1, :dish 3}
;;;    {:restaurant 2, :customer 2, :table 2, :dish 0}
;;;    {:restaurant 2, :customer 3, :table 3, :dish 1}
;;;    {:restaurant 2, :customer 4, :table 3, :dish 1}
;;;    {:restaurant 2, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 8, :table 2, :dish 0}
;;;    {:restaurant 2, :customer 9, :table 0, :dish 0}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 4, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 5, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 6, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 2}
;;;    {:restaurant 1, :customer 1, :table 1, :dish 3}
;;;    {:restaurant 1, :customer 2, :table 0, :dish 2}
;;;    {:restaurant 1, :customer 3, :table 1, :dish 3}
;;;    {:restaurant 1, :customer 4, :table 2, :dish 3}
;;;    {:restaurant 1, :customer 5, :table 1, :dish 3}
;;;    {:restaurant 1, :customer 6, :table 1, :dish 3}
;;;    {:restaurant 1, :customer 7, :table 1, :dish 3}
;;;    {:restaurant 1, :customer 8, :table 0, :dish 2}
;;;    {:restaurant 1, :customer 9, :table 1, :dish 3}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 4}
;;;    {:restaurant 2, :customer 1, :table 1, :dish 1}
;;;    {:restaurant 2, :customer 2, :table 0, :dish 4}
;;;    {:restaurant 2, :customer 3, :table 2, :dish 3}
;;;    {:restaurant 2, :customer 4, :table 0, :dish 4}
;;;    {:restaurant 2, :customer 5, :table 1, :dish 1}
;;;    {:restaurant 2, :customer 6, :table 2, :dish 3}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 4}
;;;    {:restaurant 2, :customer 8, :table 1, :dish 1}
;;;    {:restaurant 2, :customer 9, :table 0, :dish 4}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 2, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 3, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 4, :table 2, :dish 0}
;;;    {:restaurant 1, :customer 5, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 6, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 7, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 9, :table 1, :dish 1}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 1, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 2, :table 1, :dish 2}
;;;    {:restaurant 2, :customer 3, :table 2, :dish 2}
;;;    {:restaurant 2, :customer 4, :table 3, :dish 0}
;;;    {:restaurant 2, :customer 5, :table 1, :dish 2}
;;;    {:restaurant 2, :customer 6, :table 1, :dish 2}
;;;    {:restaurant 2, :customer 7, :table 1, :dish 2}
;;;    {:restaurant 2, :customer 8, :table 4, :dish 2}
;;;    {:restaurant 2, :customer 9, :table 1, :dish 2}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 6, :table 2, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 3, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 3, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 1, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 2, :table 2, :dish 0}
;;;    {:restaurant 1, :customer 3, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 4, :table 2, :dish 0}
;;;    {:restaurant 1, :customer 5, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 6, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 7, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 8, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 9, :table 0, :dish 1}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 1, :table 1, :dish 0}
;;;    {:restaurant 2, :customer 2, :table 2, :dish 1}
;;;    {:restaurant 2, :customer 3, :table 2, :dish 1}
;;;    {:restaurant 2, :customer 4, :table 1, :dish 0}
;;;    {:restaurant 2, :customer 5, :table 3, :dish 0}
;;;    {:restaurant 2, :customer 6, :table 2, :dish 1}
;;;    {:restaurant 2, :customer 7, :table 1, :dish 0}
;;;    {:restaurant 2, :customer 8, :table 2, :dish 1}
;;;    {:restaurant 2, :customer 9, :table 1, :dish 0}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 1, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 2, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 3, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 4, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 5, :table 2, :dish 1}
;;;    {:restaurant 1, :customer 6, :table 3, :dish 1}
;;;    {:restaurant 1, :customer 7, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 8, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 9, :table 0, :dish 1}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 6, :table 1, :dish 1}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 9, :table 1, :dish 1}]})
;;; 
;; <-
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; Now we implement crf using stateful objects.
;; **

;; @@
(defm hcrp-create
  [alpha parent-id]
  (let [id (gensym "hcrp")
        new-crp-obj [(CRP alpha) {} parent-id]]
    (store id new-crp-obj)
    id))

(defm hcrp-sample
  [id]
  (let [[proc table-dishes parent-id] (retrieve id)
        new-value (sample (produce proc))
        new-proc (absorb proc new-value)]
    (if (or (nil? parent-id) (contains? table-dishes new-value))
      (let [new-hcrp-obj [new-proc table-dishes parent-id]]
        (store id new-hcrp-obj)
        new-value)
      (let [new-dish (hcrp-sample parent-id)
            new-table-dishes (assoc table-dishes new-value new-dish)
            new-hcrp-obj [new-proc new-table-dishes parent-id]]
        (store id new-hcrp-obj)
        new-value))))

(defm hcrp-get-dish
  [id value]
  (let [[proc table-dishes parent-id] (retrieve id)]
    (if (contains? table-dishes value)
      (get table-dishes value)
      nil)))

(defquery crf-sampler2 [N M hyperparams]
  (let [alpha (:alpha hyperparams 1.0)
        beta (:beta hyperparams 1.0)
        menu-crp-id (hcrp-create alpha nil)
        restaurant-crp-ids (vec (repeatedly N #(hcrp-create beta menu-crp-id)))]
    
    (loop [ir 0              ; restaurant
           ic 0              ; customer
           dinners []]
      
      (if (= ir N)
        (predict :dinners dinners)
        (if (= ic M)
          (recur (inc ir) 0 dinners)
          (let [crp-id (get restaurant-crp-ids ir)
                it (hcrp-sample crp-id)
                dish (hcrp-get-dish crp-id it)
                dinners (conj dinners {:restaurant ir
                                       :customer ic 
                                       :table it 
                                       :dish dish})]
            (recur ir (inc ic) dinners)))))))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/hcrp-create</span>","value":"#'crf-in-anglican/hcrp-create"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/hcrp-sample</span>","value":"#'crf-in-anglican/hcrp-sample"}],"value":"[#'crf-in-anglican/hcrp-create,#'crf-in-anglican/hcrp-sample]"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/hcrp-get-dish</span>","value":"#'crf-in-anglican/hcrp-get-dish"}],"value":"[[#'crf-in-anglican/hcrp-create,#'crf-in-anglican/hcrp-sample],#'crf-in-anglican/hcrp-get-dish]"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/crf-sampler2</span>","value":"#'crf-in-anglican/crf-sampler2"}],"value":"[[[#'crf-in-anglican/hcrp-create,#'crf-in-anglican/hcrp-sample],#'crf-in-anglican/hcrp-get-dish],#'crf-in-anglican/crf-sampler2]"}
;; <=

;; @@
(def samples (doquery :importance crf-sampler2 [3 10]))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/samples</span>","value":"#'crf-in-anglican/samples"}
;; <=

;; @@
(def dinners (map get-predicts (take 10 samples)))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/dinners</span>","value":"#'crf-in-anglican/dinners"}
;; <=

;; @@
(clojure.pprint/pprint dinners)
;; @@
;; ->
;;; ({:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 1, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 2, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 3, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 4, :table 1, :dish 2}
;;;    {:restaurant 1, :customer 5, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 6, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 7, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 8, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 9, :table 0, :dish 1}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 1, :table 1, :dish 0}
;;;    {:restaurant 2, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 9, :table 0, :dish 0}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 6, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 9, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 1, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 2, :table 1, :dish 2}
;;;    {:restaurant 2, :customer 3, :table 2, :dish 0}
;;;    {:restaurant 2, :customer 4, :table 0, :dish 2}
;;;    {:restaurant 2, :customer 5, :table 3, :dish 2}
;;;    {:restaurant 2, :customer 6, :table 4, :dish 3}
;;;    {:restaurant 2, :customer 7, :table 2, :dish 0}
;;;    {:restaurant 2, :customer 8, :table 4, :dish 3}
;;;    {:restaurant 2, :customer 9, :table 0, :dish 2}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 1, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 2, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 3, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 5, :table 2, :dish 1}
;;;    {:restaurant 1, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 7, :table 2, :dish 1}
;;;    {:restaurant 1, :customer 8, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 9, :table 2, :dish 1}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 1, :table 1, :dish 0}
;;;    {:restaurant 2, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 3, :table 1, :dish 0}
;;;    {:restaurant 2, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 7, :table 2, :dish 1}
;;;    {:restaurant 2, :customer 8, :table 1, :dish 0}
;;;    {:restaurant 2, :customer 9, :table 0, :dish 0}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 1, :table 1, :dish 0}
;;;    {:restaurant 2, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 3, :table 1, :dish 0}
;;;    {:restaurant 2, :customer 4, :table 1, :dish 0}
;;;    {:restaurant 2, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 7, :table 2, :dish 1}
;;;    {:restaurant 2, :customer 8, :table 3, :dish 0}
;;;    {:restaurant 2, :customer 9, :table 4, :dish 0}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 6, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 2, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 4, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 1, :table 1, :dish 0}
;;;    {:restaurant 2, :customer 2, :table 2, :dish 0}
;;;    {:restaurant 2, :customer 3, :table 1, :dish 0}
;;;    {:restaurant 2, :customer 4, :table 2, :dish 0}
;;;    {:restaurant 2, :customer 5, :table 3, :dish 0}
;;;    {:restaurant 2, :customer 6, :table 3, :dish 0}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 9, :table 3, :dish 0}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 2, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 6, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 9, :table 0, :dish 0}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 6, :table 2, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 1, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 2, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 3, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 4, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 5, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 6, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 7, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 8, :table 0, :dish 1}
;;;    {:restaurant 1, :customer 9, :table 0, :dish 1}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 1, :table 1, :dish 0}
;;;    {:restaurant 2, :customer 2, :table 2, :dish 2}
;;;    {:restaurant 2, :customer 3, :table 3, :dish 0}
;;;    {:restaurant 2, :customer 4, :table 4, :dish 0}
;;;    {:restaurant 2, :customer 5, :table 5, :dish 1}
;;;    {:restaurant 2, :customer 6, :table 2, :dish 2}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 8, :table 2, :dish 2}
;;;    {:restaurant 2, :customer 9, :table 5, :dish 1}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 6, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 1, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 1, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 2, :table 2, :dish 0}
;;;    {:restaurant 1, :customer 3, :table 2, :dish 0}
;;;    {:restaurant 1, :customer 4, :table 3, :dish 0}
;;;    {:restaurant 1, :customer 5, :table 4, :dish 0}
;;;    {:restaurant 1, :customer 6, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 7, :table 5, :dish 0}
;;;    {:restaurant 1, :customer 8, :table 5, :dish 0}
;;;    {:restaurant 1, :customer 9, :table 5, :dish 0}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 2, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 9, :table 0, :dish 0}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 2, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 3, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 4, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 5, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 6, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 9, :table 0, :dish 0}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 2}
;;;    {:restaurant 1, :customer 1, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 2, :table 2, :dish 2}
;;;    {:restaurant 1, :customer 3, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 4, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 5, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 6, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 7, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 8, :table 1, :dish 0}
;;;    {:restaurant 1, :customer 9, :table 3, :dish 3}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 4}
;;;    {:restaurant 2, :customer 1, :table 0, :dish 4}
;;;    {:restaurant 2, :customer 2, :table 1, :dish 1}
;;;    {:restaurant 2, :customer 3, :table 0, :dish 4}
;;;    {:restaurant 2, :customer 4, :table 2, :dish 0}
;;;    {:restaurant 2, :customer 5, :table 0, :dish 4}
;;;    {:restaurant 2, :customer 6, :table 0, :dish 4}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 4}
;;;    {:restaurant 2, :customer 8, :table 0, :dish 4}
;;;    {:restaurant 2, :customer 9, :table 0, :dish 4}]}
;;;  {:dinners
;;;   [{:restaurant 0, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 2, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 4, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 5, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 6, :table 1, :dish 1}
;;;    {:restaurant 0, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 0, :customer 8, :table 2, :dish 1}
;;;    {:restaurant 0, :customer 9, :table 1, :dish 1}
;;;    {:restaurant 1, :customer 0, :table 0, :dish 2}
;;;    {:restaurant 1, :customer 1, :table 0, :dish 2}
;;;    {:restaurant 1, :customer 2, :table 0, :dish 2}
;;;    {:restaurant 1, :customer 3, :table 1, :dish 2}
;;;    {:restaurant 1, :customer 4, :table 1, :dish 2}
;;;    {:restaurant 1, :customer 5, :table 1, :dish 2}
;;;    {:restaurant 1, :customer 6, :table 0, :dish 2}
;;;    {:restaurant 1, :customer 7, :table 2, :dish 1}
;;;    {:restaurant 1, :customer 8, :table 1, :dish 2}
;;;    {:restaurant 1, :customer 9, :table 1, :dish 2}
;;;    {:restaurant 2, :customer 0, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 1, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 2, :table 1, :dish 1}
;;;    {:restaurant 2, :customer 3, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 4, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 5, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 6, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 7, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 8, :table 0, :dish 0}
;;;    {:restaurant 2, :customer 9, :table 1, :dish 1}]})
;;; 
;; <-
;; =>
;;; {"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}
;; <=

;; **
;;; ## Exchangeable Random Processes from Orbanz and Roy's paper 1: Infinite Relational Model
;;; 
;;; Sam asked me: how to ensure that a stateful random process is exchangeable? One approach to this question is to implement a few examples of exchangeable stateful random processes, to identify some common programming pattern, and to show that whenever the pattern is followed, we get exchangeable random processes. I will follow this approach and implement exchangeable random processes from Orbanz and Roy's paper. The first object is the simple cluster object part of the infinite relational model. I will use irm for this simple cluster object, and firm for the infinite relational object.
;;; 
;;; Let me first implement a data type for a finite partial map from natural numbers to values such that the domain of the map has the form {1,...,N} for some N, and the map supports efficience accesses to the domain, the range and the N+1. 
;; **

;; @@
(defm tracked-map
  []
  {:map   {} 
   :next  1
   :range #{}})

(defm tracked-map-extend
  [m v]
  (let [f (:map m)
        i (:next m)
        r (:range m)]
    {:map   (assoc f i v)
     :next  (inc i)
     :range (conj r v)}))

(defm tracked-map-get
  [m i]
  (get (:map m) i))

(defm tracked-map-get-range
  [m]
  (:range m))

(defm tracked-map-in-dom?
  [m i]
  (and (< 0 i) (< i (:next m))))

(defm tracked-map-in-range?
  [m v]
  (contains? (:range m) v))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/tracked-map</span>","value":"#'crf-in-anglican/tracked-map"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/tracked-map-extend</span>","value":"#'crf-in-anglican/tracked-map-extend"}],"value":"[#'crf-in-anglican/tracked-map,#'crf-in-anglican/tracked-map-extend]"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/tracked-map-get</span>","value":"#'crf-in-anglican/tracked-map-get"}],"value":"[[#'crf-in-anglican/tracked-map,#'crf-in-anglican/tracked-map-extend],#'crf-in-anglican/tracked-map-get]"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/tracked-map-get-range</span>","value":"#'crf-in-anglican/tracked-map-get-range"}],"value":"[[[#'crf-in-anglican/tracked-map,#'crf-in-anglican/tracked-map-extend],#'crf-in-anglican/tracked-map-get],#'crf-in-anglican/tracked-map-get-range]"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/tracked-map-in-dom?</span>","value":"#'crf-in-anglican/tracked-map-in-dom?"}],"value":"[[[[#'crf-in-anglican/tracked-map,#'crf-in-anglican/tracked-map-extend],#'crf-in-anglican/tracked-map-get],#'crf-in-anglican/tracked-map-get-range],#'crf-in-anglican/tracked-map-in-dom?]"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/tracked-map-in-range?</span>","value":"#'crf-in-anglican/tracked-map-in-range?"}],"value":"[[[[[#'crf-in-anglican/tracked-map,#'crf-in-anglican/tracked-map-extend],#'crf-in-anglican/tracked-map-get],#'crf-in-anglican/tracked-map-get-range],#'crf-in-anglican/tracked-map-in-dom?],#'crf-in-anglican/tracked-map-in-range?]"}
;; <=

;; @@
(defm irm-create
  [alpha beta dist]
  (let [id               (gensym "irm")
        row-crp          (CRP alpha)
        row-map          (tracked-map)
        col-crp          (CRP beta)
        col-map          (tracked-map)
        matrix           {}
        irm-obj          {:row-crp    row-crp
                          :row-map    row-map
                          :col-crp    col-crp
                          :col-map    col-map
                          :matrix     matrix
                          :dist       dist}]
    (store id irm-obj)
    id))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/irm-create</span>","value":"#'crf-in-anglican/irm-create"}
;; <=

;; @@
(defm irm-lookup
  [id i j]
  (let [irm-obj (retrieve id)]
    (if (nil? irm-obj) nil
      (let [row-map (:row-map irm-obj)
            col-map (:col-map irm-obj)
            matrix  (:matrix irm-obj)
            mi      (tracked-map-get row-map i)
            mj      (tracked-map-get col-map j)]
        (if (or (nil? mi) (nil? mj))
          nil
          (get matrix [mi mj]))))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/irm-lookup</span>","value":"#'crf-in-anglican/irm-lookup"}
;; <=

;; @@
(defm irm-new-row
  [id]
  (let [irm-obj (retrieve id)]
    (if (nil? irm-obj) nil
      (let [row-crp       (:row-crp irm-obj)
            row-map       (:row-map irm-obj)
            new-value     (sample (produce row-crp))
            new-row-crp   (absorb row-crp new-value)]
        (if (tracked-map-in-range? row-map new-value)
          (let [new-row-map (tracked-map-extend row-map new-value) 
                new-irm-obj (assoc irm-obj 
                              :row-crp new-row-crp
                              :row-map new-row-map)]
            (store id new-irm-obj)
            new-value)
          (let [new-row-map (tracked-map-extend row-map new-value)
                cols        (tracked-map-get-range (:col-map irm-obj))
                matrix      (:matrix irm-obj)
                new-matrix  (reduce 
                               (fn [m c] 
                                 (let [idx [new-value c]
                                       v   (sample (:dist irm-obj))]
                                   (assoc m idx v)))
                              matrix
                              cols)
                new-irm-obj (assoc irm-obj 
                              :row-crp new-row-crp
                              :row-map new-row-map
                              :matrix  new-matrix)]
            (store id new-irm-obj)
            new-value))))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/irm-new-row</span>","value":"#'crf-in-anglican/irm-new-row"}
;; <=

;; @@
(defm irm-new-col
  [id]
  (let [irm-obj (retrieve id)]
    (if (nil? irm-obj) nil
      (let [col-crp       (:col-crp irm-obj)
            col-map       (:col-map irm-obj)
            new-value     (sample (produce col-crp))
            new-col-crp   (absorb col-crp new-value)]
        (if (tracked-map-in-range? col-map new-value)
          (let [new-col-map (tracked-map-extend col-map new-value) 
                new-irm-obj (assoc irm-obj 
                              :col-crp new-col-crp
                              :col-map new-col-map)]
            (store id new-irm-obj)
            new-value)
          (let [new-col-map (tracked-map-extend col-map new-value)
                rows        (tracked-map-get-range (:row-map irm-obj))
                matrix      (:matrix irm-obj)
                new-matrix  (reduce 
                               (fn [m r] 
                                 (let [idx [r new-value]
                                       v   (sample (:dist irm-obj))]
                                   (assoc m idx v)))
                              matrix
                              rows)
                new-irm-obj (assoc irm-obj 
                              :col-crp new-col-crp
                              :col-map new-col-map
                              :matrix  new-matrix)]
            (store id new-irm-obj)
            new-value))))))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/irm-new-col</span>","value":"#'crf-in-anglican/irm-new-col"}
;; <=

;; @@
(defquery irm-sampler [R C]
  (let [alpha0     1.0
        beta0      1.0
        dist       (beta 1 2)
        irm-id     (irm-create alpha0 beta0 dist)
        row-tables (repeatedly R #(irm-new-row irm-id))
        col-tables (repeatedly C #(irm-new-col irm-id))
        matrix     (reduce
                     (fn [m1 r]
                       (reduce 
                         (fn [m2 c] (assoc m2 [r c] (irm-lookup irm-id r c)))
                         m1 (range 1 (+ 1 C))))
                     {} (range 1 (+ 1 R)))]
    (predict :result {:mat matrix
                      :col col-tables
                      :row row-tables})))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/irm-sampler</span>","value":"#'crf-in-anglican/irm-sampler"}
;; <=

;; @@
(def samples (doquery :importance irm-sampler [2 6]))
(def matrices (map get-predicts (take 10 samples)))
(clojure.pprint/pprint matrices)
;; @@
;; ->
;;; ({:result
;;;   {:mat
;;;    {[2 2] 0.03825208049884242,
;;;     [2 3] 0.03825208049884242,
;;;     [2 5] 0.03825208049884242,
;;;     [1 1] 0.03825208049884242,
;;;     [1 4] 0.03825208049884242,
;;;     [1 3] 0.03825208049884242,
;;;     [1 5] 0.03825208049884242,
;;;     [2 4] 0.03825208049884242,
;;;     [2 1] 0.03825208049884242,
;;;     [1 6] 0.34771782887135544,
;;;     [2 6] 0.34771782887135544,
;;;     [1 2] 0.03825208049884242},
;;;    :col (0 0 0 0 0 1),
;;;    :row (0 0)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0.5184320657210469,
;;;     [2 3] 0.5255822694143869,
;;;     [2 5] 0.09087880359081044,
;;;     [1 1] 0.5255822694143869,
;;;     [1 4] 0.5255822694143869,
;;;     [1 3] 0.5255822694143869,
;;;     [1 5] 0.09087880359081044,
;;;     [2 4] 0.5255822694143869,
;;;     [2 1] 0.5255822694143869,
;;;     [1 6] 0.09087880359081044,
;;;     [2 6] 0.09087880359081044,
;;;     [1 2] 0.5184320657210469},
;;;    :col (0 1 0 0 2 2),
;;;    :row (0 0)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0.5493164853342106,
;;;     [2 3] 0.5493164853342106,
;;;     [2 5] 0.2670105471038852,
;;;     [1 1] 0.5493164853342106,
;;;     [1 4] 0.5493164853342106,
;;;     [1 3] 0.5493164853342106,
;;;     [1 5] 0.2670105471038852,
;;;     [2 4] 0.5493164853342106,
;;;     [2 1] 0.5493164853342106,
;;;     [1 6] 0.2670105471038852,
;;;     [2 6] 0.2670105471038852,
;;;     [1 2] 0.5493164853342106},
;;;    :col (0 0 0 0 1 1),
;;;    :row (0 0)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0.3072364827557075,
;;;     [2 3] 0.3072364827557075,
;;;     [2 5] 0.3072364827557075,
;;;     [1 1] 0.3072364827557075,
;;;     [1 4] 0.3072364827557075,
;;;     [1 3] 0.3072364827557075,
;;;     [1 5] 0.3072364827557075,
;;;     [2 4] 0.3072364827557075,
;;;     [2 1] 0.3072364827557075,
;;;     [1 6] 0.3072364827557075,
;;;     [2 6] 0.3072364827557075,
;;;     [1 2] 0.3072364827557075},
;;;    :col (0 0 0 0 0 0),
;;;    :row (0 0)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0.22666362428728284,
;;;     [2 3] 0.10845493847670921,
;;;     [2 5] 0.31626608673717976,
;;;     [1 1] 0.5108173248719533,
;;;     [1 4] 0.5108173248719533,
;;;     [1 3] 0.10845493847670921,
;;;     [1 5] 0.31626608673717976,
;;;     [2 4] 0.5108173248719533,
;;;     [2 1] 0.5108173248719533,
;;;     [1 6] 0.22666362428728284,
;;;     [2 6] 0.22666362428728284,
;;;     [1 2] 0.22666362428728284},
;;;    :col (0 1 2 0 3 1),
;;;    :row (0 0)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0.03610943355838376,
;;;     [2 3] 0.4799937409166586,
;;;     [2 5] 0.17538411880713883,
;;;     [1 1] 0.08827701561466107,
;;;     [1 4] 0.5259513616032389,
;;;     [1 3] 0.08827701561466107,
;;;     [1 5] 0.09872216258187323,
;;;     [2 4] 0.579111874672663,
;;;     [2 1] 0.4799937409166586,
;;;     [1 6] 0.08827701561466107,
;;;     [2 6] 0.4799937409166586,
;;;     [1 2] 0.5550524785900273},
;;;    :col (0 1 0 2 3 0),
;;;    :row (0 1)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0.13459316443041466,
;;;     [2 3] 0.13459316443041466,
;;;     [2 5] 0.3268486986626157,
;;;     [1 1] 0.48404261138488547,
;;;     [1 4] 0.48404261138488547,
;;;     [1 3] 0.48404261138488547,
;;;     [1 5] 0.16078367473534144,
;;;     [2 4] 0.13459316443041466,
;;;     [2 1] 0.13459316443041466,
;;;     [1 6] 0.48404261138488547,
;;;     [2 6] 0.13459316443041466,
;;;     [1 2] 0.48404261138488547},
;;;    :col (0 0 0 0 1 0),
;;;    :row (0 1)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0.5727686176329699,
;;;     [2 3] 0.5727686176329699,
;;;     [2 5] 0.5727686176329699,
;;;     [1 1] 0.14366242708795107,
;;;     [1 4] 0.14366242708795107,
;;;     [1 3] 0.5727686176329699,
;;;     [1 5] 0.5727686176329699,
;;;     [2 4] 0.14366242708795107,
;;;     [2 1] 0.14366242708795107,
;;;     [1 6] 0.5727686176329699,
;;;     [2 6] 0.5727686176329699,
;;;     [1 2] 0.5727686176329699},
;;;    :col (0 1 1 0 1 1),
;;;    :row (0 0)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0.06608062829746737,
;;;     [2 3] 0.06608062829746737,
;;;     [2 5] 0.06608062829746737,
;;;     [1 1] 0.06608062829746737,
;;;     [1 4] 0.06608062829746737,
;;;     [1 3] 0.06608062829746737,
;;;     [1 5] 0.06608062829746737,
;;;     [2 4] 0.06608062829746737,
;;;     [2 1] 0.06608062829746737,
;;;     [1 6] 0.06608062829746737,
;;;     [2 6] 0.06608062829746737,
;;;     [1 2] 0.06608062829746737},
;;;    :col (0 0 0 0 0 0),
;;;    :row (0 0)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0.36035540640107855,
;;;     [2 3] 0.36035540640107855,
;;;     [2 5] 0.36035540640107855,
;;;     [1 1] 0.697509004416267,
;;;     [1 4] 0.697509004416267,
;;;     [1 3] 0.697509004416267,
;;;     [1 5] 0.697509004416267,
;;;     [2 4] 0.36035540640107855,
;;;     [2 1] 0.36035540640107855,
;;;     [1 6] 0.697509004416267,
;;;     [2 6] 0.36035540640107855,
;;;     [1 2] 0.697509004416267},
;;;    :col (0 0 0 0 0 0),
;;;    :row (0 1)}})
;;; 
;; <-
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/samples</span>","value":"#'crf-in-anglican/samples"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/matrices</span>","value":"#'crf-in-anglican/matrices"}],"value":"[#'crf-in-anglican/samples,#'crf-in-anglican/matrices]"},{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}],"value":"[[#'crf-in-anglican/samples,#'crf-in-anglican/matrices],nil]"}
;; <=

;; **
;;; Now we use the routines defined so far and define the ones for the infinite relational model. The reason that we separate out this part is that the part is somewhat generic. In fact, this is the construction in Roy and Orbanz's paper for converting a simple cluster-based random process to a cluster-based random process.
;; **

;; @@
(defm firm-create-sub
  [irm-id dist]
  (let [id       (gensym "firm")
        matrix   {}
        firm-obj {:irm-id irm-id
                  :matrix matrix    
                  :dist   dist}]
    (store id firm-obj)
    id))

(defm firm-create
  [alpha beta dist1 dist2]
  (firm-create-sub (irm-create alpha beta dist1) dist2))

(defm firm-new-row
  [id]
  (let [firm-obj (retrieve id)
        irm-id   (:irm-id firm-obj)]
    (irm-new-row irm-id)))

(defm firm-new-col
  [id]
  (let [firm-obj (retrieve id)
        irm-id   (:irm-id firm-obj)]
    (irm-new-col irm-id)))

(defm firm-lookup
  [id i j]
  (let [firm-obj (retrieve id)]
    (if (nil? firm-obj) nil
      (let [irm-id   (:irm-id firm-obj)
            matrix   (:matrix firm-obj)
            dist     (:dist firm-obj)
            theta    (irm-lookup irm-id i j)]
        (if (nil? theta) nil
          (let [idx   [i j]
                value (get matrix idx)]
            (if (not (nil? value)) value
              (let [new-value    (sample (dist theta))
                    new-matrix   (assoc matrix idx new-value)
                    new-firm-obj (assoc firm-obj :matrix new-matrix)]
                (store id new-firm-obj)
                new-value))))))))
;; @@
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/firm-create-sub</span>","value":"#'crf-in-anglican/firm-create-sub"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/firm-create</span>","value":"#'crf-in-anglican/firm-create"}],"value":"[#'crf-in-anglican/firm-create-sub,#'crf-in-anglican/firm-create]"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/firm-new-row</span>","value":"#'crf-in-anglican/firm-new-row"}],"value":"[[#'crf-in-anglican/firm-create-sub,#'crf-in-anglican/firm-create],#'crf-in-anglican/firm-new-row]"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/firm-new-col</span>","value":"#'crf-in-anglican/firm-new-col"}],"value":"[[[#'crf-in-anglican/firm-create-sub,#'crf-in-anglican/firm-create],#'crf-in-anglican/firm-new-row],#'crf-in-anglican/firm-new-col]"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/firm-lookup</span>","value":"#'crf-in-anglican/firm-lookup"}],"value":"[[[[#'crf-in-anglican/firm-create-sub,#'crf-in-anglican/firm-create],#'crf-in-anglican/firm-new-row],#'crf-in-anglican/firm-new-col],#'crf-in-anglican/firm-lookup]"}
;; <=

;; @@
(defquery firm-sampler [R C]
  (let [alpha0     1.0
        beta0      1.0
        dist1      (beta 1.0 2.0)
        dist2      bernoulli
        firm-id    (firm-create alpha0 beta0 dist1 dist2)
        row-tables (repeatedly R #(firm-new-row firm-id))
        col-tables (repeatedly C #(firm-new-col firm-id))
        matrix     (reduce
                     (fn [m1 r]
                       (reduce 
                         (fn [m2 c] (assoc m2 [r c] (firm-lookup firm-id r c)))
                         m1 (range 1 (+ 1 C))))
                     {} (range 1 (+ 1 R)))]
    (predict :result {:mat matrix
                      :row row-tables
                      :col col-tables})))
;; @@
;; =>
;;; {"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/firm-sampler</span>","value":"#'crf-in-anglican/firm-sampler"}
;; <=

;; @@
(def samples (doquery :importance firm-sampler [3 6]))
(def matrices (map get-predicts (take 10 samples)))
(clojure.pprint/pprint matrices)
;; @@
;; ->
;;; ({:result
;;;   {:mat
;;;    {[2 2] 0,
;;;     [2 3] 0,
;;;     [2 5] 0,
;;;     [3 3] 1,
;;;     [1 1] 1,
;;;     [3 4] 0,
;;;     [1 4] 0,
;;;     [1 3] 0,
;;;     [1 5] 0,
;;;     [2 4] 0,
;;;     [3 6] 0,
;;;     [3 1] 1,
;;;     [2 1] 1,
;;;     [1 6] 0,
;;;     [2 6] 1,
;;;     [1 2] 0,
;;;     [3 5] 0,
;;;     [3 2] 0},
;;;    :row (0 1 0),
;;;    :col (0 0 1 0 0 0)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0,
;;;     [2 3] 0,
;;;     [2 5] 0,
;;;     [3 3] 1,
;;;     [1 1] 0,
;;;     [3 4] 1,
;;;     [1 4] 0,
;;;     [1 3] 1,
;;;     [1 5] 0,
;;;     [2 4] 0,
;;;     [3 6] 1,
;;;     [3 1] 1,
;;;     [2 1] 1,
;;;     [1 6] 0,
;;;     [2 6] 0,
;;;     [1 2] 0,
;;;     [3 5] 0,
;;;     [3 2] 0},
;;;    :row (0 1 2),
;;;    :col (0 1 2 0 1 0)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 1,
;;;     [2 3] 1,
;;;     [2 5] 0,
;;;     [3 3] 0,
;;;     [1 1] 1,
;;;     [3 4] 0,
;;;     [1 4] 1,
;;;     [1 3] 1,
;;;     [1 5] 1,
;;;     [2 4] 1,
;;;     [3 6] 0,
;;;     [3 1] 1,
;;;     [2 1] 1,
;;;     [1 6] 0,
;;;     [2 6] 0,
;;;     [1 2] 1,
;;;     [3 5] 0,
;;;     [3 2] 1},
;;;    :row (0 0 0),
;;;    :col (0 0 0 0 1 1)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0,
;;;     [2 3] 0,
;;;     [2 5] 0,
;;;     [3 3] 1,
;;;     [1 1] 0,
;;;     [3 4] 1,
;;;     [1 4] 0,
;;;     [1 3] 1,
;;;     [1 5] 1,
;;;     [2 4] 0,
;;;     [3 6] 0,
;;;     [3 1] 0,
;;;     [2 1] 0,
;;;     [1 6] 1,
;;;     [2 6] 1,
;;;     [1 2] 1,
;;;     [3 5] 0,
;;;     [3 2] 1},
;;;    :row (0 0 0),
;;;    :col (0 1 2 3 1 2)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0,
;;;     [2 3] 0,
;;;     [2 5] 0,
;;;     [3 3] 0,
;;;     [1 1] 0,
;;;     [3 4] 0,
;;;     [1 4] 0,
;;;     [1 3] 0,
;;;     [1 5] 0,
;;;     [2 4] 0,
;;;     [3 6] 1,
;;;     [3 1] 0,
;;;     [2 1] 0,
;;;     [1 6] 0,
;;;     [2 6] 0,
;;;     [1 2] 0,
;;;     [3 5] 0,
;;;     [3 2] 0},
;;;    :row (0 1 0),
;;;    :col (0 1 0 0 0 1)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 1,
;;;     [2 3] 1,
;;;     [2 5] 0,
;;;     [3 3] 1,
;;;     [1 1] 1,
;;;     [3 4] 0,
;;;     [1 4] 1,
;;;     [1 3] 1,
;;;     [1 5] 1,
;;;     [2 4] 1,
;;;     [3 6] 1,
;;;     [3 1] 0,
;;;     [2 1] 0,
;;;     [1 6] 0,
;;;     [2 6] 1,
;;;     [1 2] 1,
;;;     [3 5] 0,
;;;     [3 2] 0},
;;;    :row (0 0 1),
;;;    :col (0 0 0 0 1 0)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0,
;;;     [2 3] 0,
;;;     [2 5] 0,
;;;     [3 3] 0,
;;;     [1 1] 0,
;;;     [3 4] 0,
;;;     [1 4] 0,
;;;     [1 3] 0,
;;;     [1 5] 0,
;;;     [2 4] 0,
;;;     [3 6] 0,
;;;     [3 1] 0,
;;;     [2 1] 0,
;;;     [1 6] 0,
;;;     [2 6] 0,
;;;     [1 2] 1,
;;;     [3 5] 0,
;;;     [3 2] 0},
;;;    :row (0 0 1),
;;;    :col (0 1 0 0 0 0)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0,
;;;     [2 3] 0,
;;;     [2 5] 1,
;;;     [3 3] 1,
;;;     [1 1] 1,
;;;     [3 4] 0,
;;;     [1 4] 0,
;;;     [1 3] 0,
;;;     [1 5] 0,
;;;     [2 4] 0,
;;;     [3 6] 0,
;;;     [3 1] 1,
;;;     [2 1] 0,
;;;     [1 6] 0,
;;;     [2 6] 0,
;;;     [1 2] 0,
;;;     [3 5] 0,
;;;     [3 2] 0},
;;;    :row (0 0 0),
;;;    :col (0 0 1 0 1 1)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 1,
;;;     [2 3] 0,
;;;     [2 5] 1,
;;;     [3 3] 1,
;;;     [1 1] 0,
;;;     [3 4] 1,
;;;     [1 4] 1,
;;;     [1 3] 0,
;;;     [1 5] 0,
;;;     [2 4] 0,
;;;     [3 6] 1,
;;;     [3 1] 0,
;;;     [2 1] 0,
;;;     [1 6] 1,
;;;     [2 6] 0,
;;;     [1 2] 0,
;;;     [3 5] 0,
;;;     [3 2] 0},
;;;    :row (0 1 0),
;;;    :col (0 1 0 0 0 0)}}
;;;  {:result
;;;   {:mat
;;;    {[2 2] 0,
;;;     [2 3] 0,
;;;     [2 5] 1,
;;;     [3 3] 0,
;;;     [1 1] 1,
;;;     [3 4] 1,
;;;     [1 4] 1,
;;;     [1 3] 0,
;;;     [1 5] 1,
;;;     [2 4] 1,
;;;     [3 6] 1,
;;;     [3 1] 0,
;;;     [2 1] 1,
;;;     [1 6] 1,
;;;     [2 6] 1,
;;;     [1 2] 0,
;;;     [3 5] 1,
;;;     [3 2] 0},
;;;    :row (0 0 0),
;;;    :col (0 1 1 0 0 2)}})
;;; 
;; <-
;; =>
;;; {"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"list-like","open":"","close":"","separator":"</pre><pre>","items":[{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/samples</span>","value":"#'crf-in-anglican/samples"},{"type":"html","content":"<span class='clj-var'>#&#x27;crf-in-anglican/matrices</span>","value":"#'crf-in-anglican/matrices"}],"value":"[#'crf-in-anglican/samples,#'crf-in-anglican/matrices]"},{"type":"html","content":"<span class='clj-nil'>nil</span>","value":"nil"}],"value":"[[#'crf-in-anglican/samples,#'crf-in-anglican/matrices],nil]"}
;; <=

;; **
;;; So far I didn't worry about score. Doing this for the above firm construction is not easy. This is because to compute a score, we should integrate out all the internal choices of sample. In general, this will be impossible. However, if we focus on the conjugate-prior case, this integration might be doable. 
;;; 
;;; There are a few lessons that I learnt while implementing the above firm object. First, I used states but in a quite benign way. In all cases, my state update does not change the old state, but it merely extend the old state. In a sense, such an update is like a form of write-once assignment. Second, my construction for firm out of irm uses what Roy and Orbanz call bernoulli-randomisation. One possible issue with this construction is that how to do score is unclear. In order to get score right, we might have to focus on good conjugacy cases only. Also, we might have to impose some form of information hiding.
;; **

;; @@

;; @@
