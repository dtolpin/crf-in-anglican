(defproject gap-detect "0.1.0-SNAPSHOT"
  :description "GAP ATO case detection"
  :url "https://bitbucket.org/cyberactive/machine-risk-profile"
  :license {:name "Proprietary"
            :url "http://www.PayPal.com/"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/tools.cli "0.3.3"]
                 [org.clojure/data.csv "0.1.3"]
                 [nstools "0.2.4"]
                 [dtolpin/anglican "0.10.0-SNAPSHOT"]
                 [dtolpin/gorilla-plot "0.1.5-SNAPSHOT"]]
  :plugins [[dtolpin/lein-gorilla "0.3.7-SNAPSHOT"]]
  :resource-paths ["programs"]
  :main ^:skip-aot anglican-user.core
  :profiles {:uberjar {:aot :all}})
